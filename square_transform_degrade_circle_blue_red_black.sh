#!/bin/bash

a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30 0.35)
a_stroke_color=(ffffff 444444 dddddd 777777 aaaaaa 000000)
r=30
g=30
b=30
rnew=255

source_file="source_optimized_ungrouped.svg"
output_file="test_degrade_circle_black.svg"
if [ -f ${output_file} ]
then
  mv ${output_file} ${output_file%.*}.$(date +%Y%m%d_%H%M%S).svg
fi


while read p; do
  echo $p | grep -qE "(\s|^)style="
  if [ $? = 0 ]
  then
    style_line=$p
  else
    echo $p | grep -qE "(\s|^)d="
    if [ $? != 0 ]
    then
      echo "$p" >> ${output_file}
    fi
  fi

  echo $p | grep -qE "(\s|^)d="
  if [ $? = 0 ]
  then
    # X
    xpos=$(echo $p | awk '{print $2}' | awk -F, '{print $1}')
    echo $xpos | grep -q "e"
    if [ $? = 0 ]
    then
      xpos=0
    fi
    third_field=$(echo $p | awk '{print $3}')
    if [ $third_field = "v" ]
    then
      fourth_field=$(echo $p | awk '{print $4}')
      xpos=$(echo "$xpos+$fourth_field" | bc)
    fi
    intxpos=$(echo "$xpos/2.6458334" | bc)

    # Y
    ypos=$(echo $p | awk '{print $2}' | awk -F, '{print $2}')
    echo $ypos | grep -q "e"
    if [ $? = 0 ]
    then
      ypos=0
    fi
    fifth_field=$(echo $p | awk '{print $5}')
    if [ $fifth_field = "v" ]
    then
      sixth_field=$(echo $p | awk '{print $6}')
      ypos=$(echo "$ypos+$sixth_field" | bc)
    fi
    intypos=$(echo "$ypos/2.6458334" | bc)
    # Inversion
    intypos=$((112-intypos))

    # Centrage
    xcenter=40
    ycenter=56
    centeredxpos=$(echo "$intxpos-$xcenter" | bc)
    centeredypos=$(echo "$intypos-$ycenter" | bc)

    # Fonction cercle
    circle=$(echo "sqrt((($intxpos-$xcenter)*($intxpos-$xcenter))+(($intypos-$ycenter)*($intypos-$ycenter)))/2" | bc)
    intcenteredxpos=${centeredxpos#-}
    intcenteredypos=${centeredypos#-}
    if [ $centeredxpos -ge 0 ] && [ $centeredypos -ge 0 ]; then
      rnew=$((r+(2*circle)))
      gnew=$g
      bnew=$b
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
        else
          rnew=$((rnew -alea))
        fi
      fi
    elif [ $centeredxpos -le 0 ] && [ $centeredypos -le 0 ]; then
      rnew=$r
      gnew=$g
      bnew=$((b+(2*circle)))
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          bnew=$((bnew + alea))
        else
          bnew=$((bnew -alea))
        fi
      fi
    elif [ $centeredxpos -lt 0 ] && [ $centeredypos -gt 0 ]; then
      rnew=$((r+(2*circle)-intcenteredxpos))
      gnew=$g
      bnew=$((b+(2*circle)-intcenteredypos))
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
          bnew=$((bnew + alea))
        else
          rnew=$((rnew -alea))
          bnew=$((bnew -alea))
        fi
      fi
    elif [ $centeredxpos -gt 0 ] && [ $centeredypos -lt 0 ]; then
      rnew=$((r+(2*circle)-intcenteredypos))
      gnew=$g
      bnew=$((b+(2*circle)-intcenteredxpos))
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
          bnew=$((bnew + alea))
        else
          rnew=$((rnew -alea))
          bnew=$((bnew -alea))
        fi
      fi
    fi

    # Limits
    if [ $rnew -gt 140 ]
    then
      rnew=140
    fi
    if [ $bnew -gt 140 ]
    then
      bnew=140
    fi

    # Hexa translation
    rhex=$(echo "obase=16; $rnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
    ghex=$(echo "obase=16; $gnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
    bhex=$(echo "obase=16; $bnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

    # Write file
    echo "$style_line" | sed "s/ffffff/$rhex$ghex$bhex/g" >> ${output_file}
    echo "$p" >> ${output_file}
  fi

  #echo "$p" | sed "s/ffffff/$a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white.svg
  #echo "$p" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white_bis.svg
done < $source_file
