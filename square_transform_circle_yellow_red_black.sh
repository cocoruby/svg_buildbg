#!/bin/bash

a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30)
# Link A
#a_stroke_color=(444444 dddddd 777777 aaaaaa 000000)
a_stroke_color=(ffffff 444444 dddddd 777777 aaaaaa 000000)
r=30
g=30
b=30

source_file="base.svg"
output_file="test_circle_blue_yellow_black.svg"
width=2.5
height=2.5

page_width=300
page_height=400

xcenter=$(echo "$page_width/($width*2)" | bc)
ycenter=$(echo "$page_height/($height*2)" | bc)

if [ -f ${output_file} ]
then
  mv ${output_file} ${output_file%.*}.$(date +%Y%m%d_%H%M%S).svg
fi


while read p; do
  echo $p | grep -qE "(\s|^)style="
  if [ $? = 0 ]
  then
    style_line=$p
  else
    echo $p | grep -qE "(\s|^)d="
    if [ $? != 0 ]
    then
      echo "$p" >> ${output_file}
    fi
  fi

  echo $p | grep -qE "(\s|^)d="
  if [ $? = 0 ]
  then
    # X
    xpos=$(echo $p | awk '{print $2}' | awk -F, '{print $1}')
    #xpos=${xpos%.*}
    intxpos=$(echo "$xpos/$width" | bc)
    # Inversion
    #intxpos=$(echo "($page_width/$width)-intxpos" | bc)

    # Y
    ypos=$(echo $p | awk '{print $2}' | awk -F, '{print $2}')
    #ypos=${ypos%.*}
    intypos=$(echo "$ypos/$height" | bc)
    # Inversion
    #intypos=$(echo "($page_height/$height)-intypos" | bc)

    # Centrage
    centeredxpos=$(echo "$intxpos-$xcenter" | bc)
    centeredypos=$(echo "$intypos-$ycenter" | bc)
    intcenteredxpos=${centeredxpos#-}
    intcenteredypos=${centeredypos#-}


    ## Cercle et quadirallage ##
#    circle=$(echo "sqrt((($intxpos-$xcenter)*($intxpos-$xcenter))+(($intypos-$ycenter)*($intypos-$ycenter)))" | bc)
#    if [ $circle -lt 20 ]
#    then
#      if [ $((circle%2)) -ne 0 ]
#      then
#        rnew=0
#        gnew=0
#        bnew=0
#      else
#        rnew=255
#        gnew=255
#        bnew=255
#      fi
#    elif [ $((intxpos%2)) -ne 0 ] && [ $((intypos%2)) -ne 0 ];
#    then
#      rnew=0
#      gnew=0
#      bnew=0
#    elif [ $((intxpos%2)) -eq 0 ] && [ $((intypos%2)) -eq 0 ];
#    then
#      rnew=0
#      gnew=0
#      bnew=0
#    else
#      rnew=255
#      gnew=255
#      bnew=255
#    fi      
    ## Cercle et quadirallage ##

    ## Cercle ##
#    circle=$(echo "sqrt((($intxpos-$xcenter)*($intxpos-$xcenter))+(($intypos-$ycenter)*($intypos-$ycenter)))" | bc)
#    rnew=$((r+(20*circle)))
#    #gnew=$((g+circle))
#    gnew=$g
#    bnew=$b
#    if [ $rnew -gt 714 ]
#    then
#      if [ $((intxpos%2)) -ne 0 ] && [ $((intypos%2)) -ne 0 ];
#      then
#        rnew=0
#        gnew=0
#        bnew=0
#      elif [ $((intxpos%2)) -eq 0 ] && [ $((intypos%2)) -eq 0 ];
#      then
#        rnew=0
#        gnew=0
#        bnew=0
#      else
#        rnew=255
#        gnew=255
#        bnew=255
#      fi
#    fi      
    ## cercle ##

    ## Radius ##
    #a=2
    #c=2
    #dividor=$(echo "$intcenteredxpos+sqrt(($intcenteredxpos*$intcenteredxpos)+($intcenteredypos*$intcenteredypos))" | bc)
    #if [ $dividor -eq 0 ]
    #then
    #  dividor=1
    #fi
    #arctan=$(echo "a($intcenteredypos/($dividor))" | bc -l)
    #if [ $(echo "$arctan < 0" | bc) ]
    #then
    #  arctan=$(echo "$arctan + 2*4*a(1)" | bc -l)
    #fi
    #spirale_archimede=$(echo "$c * 2 * (180/(4*a(1))) * $arctan" | bc -l)
    #spirale_fermat=$(echo "sqrt(2 *(a($intypos/($intxpos+sqrt((($intxpos-40)*($intxpos-40))+(($intypos-56)*($intypos-56)))))))" | bc -l)
    #radius=${spirale_archimede%.*}
    #radius=$((radius-1439))
    #rnew=$((50+(7*radius)))
    #gnew=0
    #bnew=0
    ## Radius ##

    # Fonction cercle
    circle=$(echo "sqrt((($intxpos-$xcenter)*($intxpos-$xcenter))+(($intypos-$ycenter)*($intypos-$ycenter)))/2" | bc)
    if [ $centeredxpos -ge 0 ] && [ $centeredypos -ge 0 ]; then
      rnew=$((r+(2*circle)))
      gnew=$g
      bnew=$b
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
        else
          rnew=$((rnew -alea))
        fi
      fi
    elif [ $centeredxpos -le 0 ] && [ $centeredypos -le 0 ]; then
      rnew=$((r+(2*circle)))
      gnew=$((g+(2*circle)))
      bnew=$b
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
          gnew=$((gnew + alea))
        else
          rnew=$((rnew -alea))
          gnew=$((gnew -alea))
        fi
      fi
    elif [ $centeredxpos -lt 0 ] && [ $centeredypos -gt 0 ]; then
      rnew=$((r+(2*circle)))
      gnew=$((g+(2*circle)-intcenteredypos))
      bnew=$b
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
          bnew=$((bnew + alea))
        else
          rnew=$((rnew -alea))
          bnew=$((bnew -alea))
        fi
      fi
    elif [ $centeredxpos -gt 0 ] && [ $centeredypos -lt 0 ]; then
      rnew=$((r+(2*circle)))
      gnew=$((g+(2*circle)-intcenteredxpos))
      bnew=$b
      luck=$((1 + RANDOM % 10))
      if [ $luck -gt 8 ]; then
        sign=$((1 + RANDOM % 2))
        alea=$((1 + RANDOM % 15))
        if [ $sign -eq 1 ]; then
          rnew=$((rnew + alea))
          bnew=$((bnew + alea))
        else
          rnew=$((rnew -alea))
          bnew=$((bnew -alea))
        fi
      fi
    fi

    # Limits
    if [ $rnew -ge 255 ]
    then
      rnew=255
    fi
    if [ $gnew -ge 255 ]
    then
      gnew=255
    fi
    if [ $bnew -ge 255 ]
    then
      bnew=255
    fi
    if [ $rnew -le 0 ]
    then
      rnew=0
    fi
    if [ $gnew -le 0 ]
    then
      gnew=0
    fi
    if [ $bnew -le 0 ]
    then
      bnew=0
    fi

    ## Quadrillage noir/blanc
#    if [ $((intxpos%3)) -ne 0 ] && [ $((intypos%3)) -ne 0 ];
#    then
#      rnew=0
#      gnew=0
#      bnew=0
#    elif [ $((intxpos%3)) -eq 0 ] && [ $((intypos%3)) -eq 0 ];
#    then
#      rnew=0
#      gnew=0
#      bnew=0
#    else
#      rnew=255
#      gnew=255
#      bnew=255
#    fi      

    ## Couleurs coulantes R -> V -> B ##
#    while [ $rnew -gt 255 ]
#    do
#      gnew=$((rnew-255))
#      rnew=255
#      if [ $gnew -gt 255 ]
#      then
#        bnew=$((gnew-255))
#	gnew=255
#      fi
#      if [ $bnew -gt 255 ]
#      then
#	rnew=$((255-(bnew-255)))
#	bnew=255
#      fi
#      if [ $rnew -lt 0 ]
#      then
#         gnew=$((255+rnew))
#	 rnew=0
#      fi
#      if [ $gnew -lt 0 ]
#      then
#        bnew=$((255+gnew))
#	gnew=0
#      fi
#      if [ $bnew -lt 0 ]
#      then
#        rnew=$((-(bnew+255)))
#        bnew=0
#      fi
#    done
#    ## Couleurs coulantes R -> V -> B ##
    
    ## Couleurs Par tranches R puis V puis B
#    while [ $rnew -gt 255 ]
#    do
#      gnew=$((rnew-255))
#      rnew=0
#      if [ $gnew -gt 255 ]
#      then
#        bnew=$((gnew-255))
#        gnew=0
#      fi
#      if [ $bnew -gt 255 ]
#      then
#        rnew=$((bnew-255))
#        bnew=0
#      fi
#    done
    ## Couleurs Par tranches R puis V puis B

    ## Keep each color between 0 and 255 ##
#    while [ $rnew -gt 255 ]
#    do
#      rnew=$((255-(rnew-255)))
#      if [ $rnew -lt 0 ]
#      then
#        rnew=$((-(rnew+255)))
#      fi
#    done
#    while [ $gnew -gt 255 ]
#    do
#      gnew=$((255-(gnew-255)))
#      if [ $gnew -lt 0 ]
#      then
#        gnew=$((-(gnew+255)))
#      fi
#    done
#    while [ $bnew -gt 255 ]
#    do
#      bnew=$((255-(bnew-255)))
#      if [ $bnew -lt 0 ]
#      then
#        bnew=$((-(bnew+255)))
#      fi
#    done
    ## Keep each color between 0 and 255 ##

    #echo "$rnew $gnew $bnew"

    # Hexa translation
    rhex=$(echo "obase=16; $rnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
    ghex=$(echo "obase=16; $gnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
    bhex=$(echo "obase=16; $bnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

    # Write file
    # Link A
    #echo "$style_line" | sed "s/ff0000/$rhex$ghex$bhex/g" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/0.1/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> ${output_file}
    echo "$style_line" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/ff0000/$rhex$ghex$bhex/g" | sed "s/0\.1/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> ${output_file}
    echo "$p" >> ${output_file}
  fi

done < $source_file
