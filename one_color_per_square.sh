#!/bin/bash

a_fill=(07c115 07c115 07c180 07c180 07c1b1 07c1b1 079ec1 079ec1 076ac1 076ac1 0745c1 0745c1 b6c107 c10b07 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000)
a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30 0.35)
a_stroke_color=(ffffff 444444 dddddd 777777 aaaaaa 000000)
r=0
g=0
b=0


while read p; do
  echo $p | grep -qE "(\s|^)style="
  if [ $? = 0 ]
  then
    gnew=$((g+(intxpos)))
    if [ $gnew -gt 230 ]
    then
      gnew=230
      rnew=$((r+(intxpos)))
      if [ $rnew -gt 255 ]
      then
        rnew=255
      fi
    fi
    rhex=$(echo "obase=16; $rnew" | bc | tr '[:upper:]' '[:lower:]')
    ghex=$(echo "obase=16; $gnew" | bc | tr '[:upper:]' '[:lower:]')
    bhex=$(echo "obase=16; $b" | bc | tr '[:upper:]' '[:lower:]')

    echo "$p" | sed "s/ffffff/$rhex$ghex$bhex/g" >> carres_transformed_degrade.svg
  else
    echo "$p" >> carres_transformed_degrade.svg
  fi

  #echo "$p" | sed "s/ffffff/$a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white.svg
  #echo "$p" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white_bis.svg
done < carres_transformed_white_bis.svg
