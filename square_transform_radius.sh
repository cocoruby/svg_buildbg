#!/bin/bash

a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30 0.35)
a_stroke_color=(ffffff 444444 dddddd 777777 aaaaaa 000000)
r=50
g=0
b=0

source_file="source_optimized_ungrouped.svg"
output_file="test_spirale.svg"
if [ -f ${output_file} ]
then
  mv ${output_file} ${output_file%.*}.$(date +%Y%m%d_%H%M%S).svg
fi


while read p; do
  echo $p | grep -qE "(\s|^)style="
  if [ $? = 0 ]
  then
    style_line=$p
  else
    echo $p | grep -qE "(\s|^)d="
    if [ $? != 0 ]
    then
      echo "$p" >> ${output_file}
    fi
  fi

  echo $p | grep -qE "(\s|^)d="
  if [ $? = 0 ]
  then
    # X
    xpos=$(echo $p | awk '{print $2}' | awk -F, '{print $1}')
    echo $xpos | grep -q "e"
    if [ $? = 0 ]
    then
      xpos=0
    fi
    third_field=$(echo $p | awk '{print $3}')
    if [ $third_field = "v" ]
    then
      fourth_field=$(echo $p | awk '{print $4}')
      xpos=$(echo "$xpos+$fourth_field" | bc)
    fi
    intxpos=$(echo "$xpos/2.6458334" | bc)

    # Y
    ypos=$(echo $p | awk '{print $2}' | awk -F, '{print $2}')
    echo $ypos | grep -q "e"
    if [ $? = 0 ]
    then
      ypos=0
    fi
    fifth_field=$(echo $p | awk '{print $5}')
    if [ $fifth_field = "v" ]
    then
      sixth_field=$(echo $p | awk '{print $6}')
      ypos=$(echo "$ypos+$sixth_field" | bc)
    fi
    intypos=$(echo "$ypos/2.6458334" | bc)
    # Inversion
    intypos=$((112-intypos))

    # Centrage
    xcenter=40
    ycenter=56
    centeredxpos=$(echo "$intxpos-$xcenter" | bc)
    centeredypos=$(echo "$intypos-$ycenter" | bc)
    intcenteredxpos=${centeredxpos#-}
    intcenteredypos=${centeredypos#-}

    # Fonction cercle
    #circle=$(echo "sqrt((($intxpos-$xcenter)*($intxpos-$xcenter))+(($intypos-$ycenter)*($intypos-$ycenter)))/2" | bc)
    a=2
    c=2
    #spirale_archimede=$(echo "$a + $b * (2 *(a($intypos/($intxpos+sqrt((($intxpos-40)*($intxpos-40))+(($intypos-56)*($intypos-56)))))))" | bc -l)
    dividor=$(echo "$intcenteredxpos+sqrt(($intcenteredxpos*$intcenteredxpos)+($intcenteredypos*$intcenteredypos))" | bc)
    if [ $dividor -eq 0 ]
    then
      dividor=1
    fi
    arctan=$(echo "a($intcenteredypos/($dividor))" | bc -l)
    if [ $(echo "$arctan < 0" | bc) ]
    then
      arctan=$(echo "$arctan + 2*4*a(1)" | bc -l)
    fi
    #spirale_archimede=$(echo "$a + $b * (2 * 180 / 4*a(1) *($arctan))" | bc -l)
    spirale_archimede=$(echo "$c * 2 * (180/(4*a(1))) * $arctan" | bc -l)
    #spirale_fermat=$(echo "sqrt(2 *(a($intypos/($intxpos+sqrt((($intxpos-40)*($intxpos-40))+(($intypos-56)*($intypos-56)))))))" | bc -l)
    radius=${spirale_archimede%.*}
    radius=$((radius-1439))
    
    # 2 cônes
    rnew=$((50+(7*radius)))
#    rnew=$((rnew % 765))
    gnew=0
    bnew=0

    while [ $rnew -gt 255 ]
    do
      gnew=$((rnew-255))
      rnew=255
      if [ $gnew -gt 255 ]
      then
        bnew=$((gnew-255))
	gnew=255
      fi
      if [ $bnew -gt 255 ]
      then
	rnew=$((255-(bnew-255)))
	bnew=255
      fi
      if [ $rnew -lt 0 ]
      then
         gnew=$((255+rnew))
	 rnew=0
      fi
      if [ $gnew -lt 0 ]
      then
        bnew=$((255+gnew))
	gnew=0
      fi
      if [ $bnew -lt 0 ]
      then
        rnew=$((-(bnew+255)))
        bnew=0
      fi
    done


    #echo "$rnew $gnew $bnew"

    # Hexa translation
    rhex=$(echo "obase=16; $rnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
    ghex=$(echo "obase=16; $gnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
    bhex=$(echo "obase=16; $bnew" | bc | tr '[:upper:]' '[:lower:]')
    if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

    # Write file
    echo "$style_line" | sed "s/ffffff/$rhex$ghex$bhex/g" >> ${output_file}
    echo "$p" >> ${output_file}
  fi

  #echo "$p" | sed "s/ffffff/$a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white.svg
  #echo "$p" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white_bis.svg
done < $source_file
