#!/bin/bash

a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30 0.35)
a_stroke_color=(ffffff 444444 dddddd 777777 aaaaaa 000000)
r=110
g=190
b=255
rnew=110

source_file="source_optimized_ungrouped.svg"
#source_file="source_test.svg"
output_file="test_degrade.svg"
if [ -f ${output_file} ]
then
  mv ${output_file} ${output_file%.*}.$(date +%Y%m%d_%H%M%S).svg
fi


while read p; do
  echo $p | grep -qE "(\s|^)style="
  if [ $? = 0 ]
  then
    style_line=$p
  else
    echo $p | grep -qE "(\s|^)d="
    if [ $? != 0 ]
    then
      echo "$p" >> ${output_file}
    fi
  fi

  echo $p | grep -qE "(\s|^)d="
  if [ $? = 0 ]
  then
    xpos=$(echo $p | awk '{print $2}' | awk -F, '{print $1}')
    #echo "xpos : $xpos" >> ${output_file}
    echo $xpos | grep -q "e"
    if [ $? = 0 ]
    then
      #echo "xpos : $xpos" >> ${output_file}
      xpos=0
    fi
    third_field=$(echo $p | awk '{print $3}')
    if [ $third_field = "v" ]
    then
      fourth_field=$(echo $p | awk '{print $4}')
      #echo "fourth field : $fourth_field" >> ${output_file}
      #echo "fourth : $fourth_field"
      #echo "xpos : $xpos"
      xpos=$(echo "$xpos+$fourth_field" | bc)
      #echo "new xpos : $xpos"
      #echo "xpos : $xpos" >> ${output_file}
    fi
    intxpos=$(echo "$xpos/2.6458334" | bc)
    #echo "intxpos : $intxpos" >> ${output_file}

    gnew=$((g+(intxpos)))
    #echo "g : $g" >> ${output_file}
    #echo "gnew : $gnew" >> ${output_file}
    #echo "r : $r" >> ${output_file}
    if [ $gnew -gt 230 ]
    then
      gnew=230
      rnew=$((r+(g+intxpos-230)))
      #echo "rnew : $rnew" >> ${output_file}
      if [ $rnew -gt 255 ]
      then
        rnew=255
      fi
    else
      rnew=$r
    fi
    #echo "red: $rnew, green: $gnew, blue: $b"
    rhex=$(echo "obase=16; $rnew" | bc | tr '[:upper:]' '[:lower:]')
    ghex=$(echo "obase=16; $gnew" | bc | tr '[:upper:]' '[:lower:]')
    bhex=$(echo "obase=16; $b" | bc | tr '[:upper:]' '[:lower:]')


    echo "$style_line" | sed "s/ffffff/$rhex$ghex$bhex/g" >> ${output_file}
    echo "$p" >> ${output_file}
  fi

  #echo "$p" | sed "s/ffffff/$a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white.svg
  #echo "$p" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/7cb9f5/ffffff/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> carres_transformed_white_bis.svg
done < $source_file
