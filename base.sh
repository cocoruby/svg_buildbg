#!/bin/bash

page_width=300
page_height=400

width=2.5
height=2.5

output_file=base.svg

echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
<svg
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
   xmlns:cc=\"http://creativecommons.org/ns#\"
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
   xmlns:svg=\"http://www.w3.org/2000/svg\"
   xmlns=\"http://www.w3.org/2000/svg\"
   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"
   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"
   width=\"${page_width}mm\"
   height=\"${page_height}mm\"
   viewBox=\"0 0 ${page_width} ${page_height}\"
   version=\"1.1\"
   id=\"svg75394\"
   inkscape:version=\"1.0.1 (3bc2e813f5, 2020-09-07)\"
   sodipodi:docname=\"base.svg\">
  <defs
     id=\"defs75388\" />
  <sodipodi:namedview
     id=\"base\"
     pagecolor=\"#ffffff\"
     bordercolor=\"#666666\"
     borderopacity=\"1.0\"
     inkscape:pageopacity=\"0.0\"
     inkscape:pageshadow=\"2\"
     inkscape:zoom=\"0.35\"
     inkscape:cx=\"400\"
     inkscape:cy=\"560\"
     inkscape:document-units=\"mm\"
     inkscape:current-layer=\"layer1\"
     inkscape:document-rotation=\"0\"
     showgrid=\"false\"
     inkscape:window-width=\"1680\"
     inkscape:window-height=\"1016\"
     inkscape:window-x=\"1920\"
     inkscape:window-y=\"0\"
     inkscape:window-maximized=\"1\" />
  <metadata
     id=\"metadata75391\">
    <rdf:RDF>
      <cc:Work
         rdf:about=\"\">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label=\"Layer 1\"
     inkscape:groupmode=\"layer\"
     id=\"layer1\">" > $output_file

for y in $(seq 0 $height $page_height)
  do
  for x in $(seq 0 $width $page_width)
    do
    x=$(echo ${x} | tr ',' '.')
    y=$(echo ${y} | tr ',' '.')
    echo "<path 
  inkscape:connector-curvature=\"0\" 
  id=\"path${x}_${y}\" 
  style=\"fill:#ff0000;stroke:#ffffff;stroke-width:0.1;stroke-linecap:round;paint-order:normal;fill-opacity:1\" 
  d=\"M ${x},${y} h ${width} v ${height} h -${width} z\" />" >> $output_file
  done
done

echo "</g>
</svg>" >> $output_file

