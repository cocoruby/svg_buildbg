#!/bin/bash

a_stroke_width=(0.200 0.300 0.400 0.500 0.600 0.700 0.800 0.900)
a_stroke_color=(ffffff 444444 dddddd 777777 aaaaaa 000000)

r=9
g=9
b=9

while read p; do
  echo $p | grep -qE "(\s|^)style="
  if [ $? = 0 ]
  then
    if [ $b = 255 ]
    then
      b=10
      if [ $g = 255 ]
      then
        g=10
        r=$((r+1))
      else
        g=$((g+1))
      fi
    else
      b=$((b+1))
    fi

    rhex=$(echo "obase=16; $r" | bc | tr '[:upper:]' '[:lower:]')
    ghex=$(echo "obase=16; $g" | bc | tr '[:upper:]' '[:lower:]')
    bhex=$(echo "obase=16; $b" | bc | tr '[:upper:]' '[:lower:]')

    if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
    if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
    if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

    echo "$p" | sed "s/000000/${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]} ) ))]}/g" | sed "s/fadb00/$rhex$ghex$bhex/g" | sed "s/0.56692914/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" >> source_to_optimize.svg
  else
    echo "$p" >> source_to_optimize.svg
  fi
done < source2.svg
